﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	[Header("Basic settings")]
    public float Speed;
	public Rigidbody2D rb;
	public bool Control = true;

	[Header("Spanwer settings")]
	public bool up;
	public bool down;
	public bool left;
	public bool right;

	void Start()
	{
	  Control = true;
	}

	void Update()
	{
    // Movement key
    	if(Control == true)
		{
       		if((Input.GetKey(KeyCode.RightArrow)) || (Input.GetKey(KeyCode.D)))
	   		{
		   		rb.AddForce(new Vector2 (Speed,0f));
			    right = true;
	   		}
			else
			{
				right = false;
			}
			
	   		if((Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.A)))
	   		{
			   	rb.AddForce(new Vector2 (-Speed,0f));
				left = true;
	   		}
			else
			{
				left = false;  
			}

	   		if((Input.GetKey(KeyCode.UpArrow)) || (Input.GetKey(KeyCode.W)))
	   		{
			   	rb.AddForce(new Vector2 (0f,Speed));
				up = true;
	   		}
			else
			{
				up = false;
			}

	   		if((Input.GetKey(KeyCode.DownArrow)) || (Input.GetKey(KeyCode.S)))
	   		{
		 	  	rb.AddForce(new Vector2 (0f,-Speed));
				down = true;
	   		}
			else
			{
				down = false;
			}
		}
    }

	void OnTriggerStay2D(Collider2D other)
	{
		if(other.gameObject.CompareTag("Torch"))
		{
           GameObject.Find("Spawner").GetComponent<SpawnManager>().Limit = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.gameObject.CompareTag("Torch"))
		{
           GameObject.Find("Spawner").GetComponent<SpawnManager>().Limit = false;
		}
	}
}