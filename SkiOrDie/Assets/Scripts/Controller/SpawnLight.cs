﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLight : MonoBehaviour
{
    [Header("Basic settings")]
    public GameObject[] Light;
    public Transform SpawnPoint;
    public float cooldown;
    public float delay;
    public bool CanSpawn = false;

    [Header("Time settings")]
    public float time;
    public int i;

    void Start()
    {
        StartCoroutine(Delay());
    }

    void Update()
    {
        time = (int) GameObject.Find("TimerController").GetComponent<Timer>().time;

        if(time > 435)
        {
           i = 0;
        }

        if(time <= 435 && time > 375)
        {
            i = 1;
        }

        if(time <= 375 && time > 315)
        {
            i = 2;
        }

        if(time <= 315 && time > 255)
        {
            i = 3;
        }

        if(time <= 255 && time > 195)
        {
            i = 4;
        }

        if(time <= 195 && time > 135)
        {
            i = 5;
        }

        if(time <= 135 && time > 75)
        {
            i = 6;
        }

        if(time <= 75 && time > 15)
        {
            i = 7;
        }

        if(time < 15)
        {
            i = 8;
        }

        if(GameObject.Find("WebcamLight").GetComponent<WebcamController>().inputLight == true && CanSpawn == true)
        {
           GameObject.Destroy(GameObject.FindGameObjectWithTag("Light"));
           Instantiate(Light[i], SpawnPoint.position, SpawnPoint.rotation);
           StartCoroutine(Spawn());
        }
    }

    IEnumerator Spawn()
    {
        CanSpawn = false;
        yield return new WaitForSeconds(cooldown);
        CanSpawn = true;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(delay);
        CanSpawn = true;
    }
}