﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebcamCalibrator : MonoBehaviour {
	WebcamController webcamController;
	// Use this for initialization
	bool samplingDark;
	bool samplingLight;
	float darkSum;
	int darkCount;
	float lightSum;
	int lightCount;
	void Start () {
			
	}
	
	// Update is called once per frame
	void Update () {
		webcamController = gameObject.GetComponent<WebcamController> ();
	}

	public void calibrateDark(){
		webcamController.enabled = true;
		samplingDark = true;
		StartCoroutine (sampleDark ());
	}

	public void endDarkCalibration(){
		samplingDark = false;
	}

	IEnumerator sampleDark(){
		while (samplingDark) {
			darkSum += webcamController.inputLightLevel;
			darkCount++;
			yield return new WaitForSeconds (0.05f);
		}
	}

	public void calibrateLight(){
		samplingLight = true;
		StartCoroutine (sampleLight ());
	}

	IEnumerator sampleLight(){
		while (samplingLight) {
			lightSum += webcamController.inputLightLevel;
			lightCount++;
			yield return new WaitForSeconds (0.05f);
		}
	}

	public void endCalibration(){
		samplingLight = false;
		WebcamController.treshold = (lightSum / lightCount + darkSum / darkCount) / 2;
		WebcamController.webcamTexture.Stop ();
	}
}
