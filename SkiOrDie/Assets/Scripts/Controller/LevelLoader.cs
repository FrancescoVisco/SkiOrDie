﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float transitionTime;
    public bool Fade = false;
    public int ThisScene;
    public GameObject CanvasEnd;
    
    void Start()
    {
      ThisScene = SceneManager.GetActiveScene().buildIndex;
    }

    void Update()
    {

       if(Fade == true && ThisScene == 0)
       {
          LoadNextLevel();
          transition.SetBool("Fade", true);
       }

       if(Fade == true && ThisScene == 1)
       {
          LoadMenu();
          transition.SetBool("Fade", true);
          StartCoroutine(DelayCanvasEnd());
       }

       if(ThisScene == 0)
       {
         transitionTime = 2.5f;
       }

       if(ThisScene == 1)
       {
         transitionTime = 10f;
       }
    }

    public void LoadNextLevel()
    {
      StartCoroutine(LoadLevel(1));     
    }

    public void LoadMenu()
    {
      StartCoroutine(LoadLevel(0));     
    }

    IEnumerator LoadLevel(int LevelIndex)
    {  
      yield return new WaitForSeconds(transitionTime);
      SceneManager.LoadScene(LevelIndex);
    }

    IEnumerator DelayCanvasEnd()
    {
      yield return new WaitForSeconds(3f);
      CanvasEnd.SetActive(true);
    }
}
