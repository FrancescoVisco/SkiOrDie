﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebcamController : MonoBehaviour {
	public int pixelSkip;
	public int requestedFPS;
	public static float treshold;
	public bool inputLight;
	public static WebCamTexture webcamTexture;

	Light light;
	public float inputLightLevel = 0;
	public float MaxInput;

	void Start () 
	{
		treshold = MaxInput;
		if (webcamTexture == null) {
			webcamTexture = new WebCamTexture ();
			webcamTexture.requestedFPS = requestedFPS;
			webcamTexture.Play ();
		} else {
			webcamTexture.Stop ();
			webcamTexture.Play ();
		}
	}

	void Update () 
	{
		if (WebcamController.webcamTexture.isPlaying) {
			Color[] colors = WebcamController.webcamTexture.GetPixels ();
			float sum = 0;
			int pixelChecked = Mathf.FloorToInt (colors.Length / pixelSkip);
			for (int i = 0; i < pixelChecked; i += pixelSkip) {
				sum += colors [i].grayscale;
				//print ("grayscale level of " + i + ": " + colors [i].grayscale);
			}
			inputLightLevel = sum * pixelSkip / pixelChecked;
			//print ("sum: "+sum+"  pixelChecked: "+pixelChecked+"  colors.Length: "+colors.Length+"  inputLight: "+inputLight);
			inputLight = inputLightLevel > treshold;
		}
	}
}
