﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public Transform[] SpawnPoint;
    public GameObject[] Structure;

    public bool TorchOn = false;
    public bool StructureOn = false;
    public bool PlayerInStructure = false;
    public bool Limit;
    public float delayt;
    public float cooldownt;
    public float delayinits;
    public float delays;
    public float cooldowns;

    [Header("Spawner settings")]
    public bool up;
    public bool down;
    public bool left;
    public bool right;
    private int i;
    public int s;
    public int time;
    

    void Start()
    {
        StartCoroutine(DelayTorch());
        StartCoroutine(DelayStructure());
    }

    void Update()
    {
        //Calcolo Time
        time = (int) GameObject.Find("TimerController").GetComponent<Timer>().time;

        //Calcolo SpawnPoint
        up = GameObject.Find("Player").GetComponent<PlayerController>().up;
        down = GameObject.Find("Player").GetComponent<PlayerController>().down;
        left = GameObject.Find("Player").GetComponent<PlayerController>().left;
        right = GameObject.Find("Player").GetComponent<PlayerController>().right;

        if(up == true && right == false && down == false && left == false)
        {
            i = 0;
        }
        if(up == true && right == true && down == false && left == false)
        {
            i = 1;
        }
        if(up == false && right == true && down == false && left == false)
        {
            i = 2;
        }
        if(up == false && right == true && down == true && left == false)
        {
            i = 3;
        }
        if(up == false && right == false && down == true && left == false)
        {
            i = 4;
        }
        if(up == false && right == false && down == true && left == true)
        {
            i = 5;
        }
        if(up == false && right == false && down == false && left == true)
        {
            i = 6;
        }
        if(up == true && right == false && down == false && left == true)
        {
            i = 7;
        }

        //Calcolo struttura da spawnare
        if(time <= 500 && time > 437)
        {
            s = 1;
        }

        if(time <= 437 && time > 376)
        {
            s = 2;
        }

        if(time <= 376 && time > 315)
        {
            s = 3;
        }

        if(time <= 315 && time > 254)
        {
            s = 4;
        }

        if(time <= 254 && time > 193)
        {
            s = 5;
        }

        if(time <= 193 && time > 132)
        {
            s = 6;
        }

        if(time <= 132 && time > 71)
        {
            s = 7;
        }

        if(time <= 71 && time > 0)
        {
            s = 8;
        }



        //Calcolo cooldown
        if(s == 0)
        {
            cooldowns = 20f;
        }
        else
        {
            cooldowns = 40f;
        }

        //TorchLight spawner
        if(time < 498f && TorchOn == true && Limit == false &&  PlayerInStructure == false)
        {
            Instantiate(Structure[0], SpawnPoint[i].position, SpawnPoint[i].rotation);
            StartCoroutine(SpawnTorch());
        }

        if(time < 490f && StructureOn == true)
        {
            
            Instantiate(Structure[s], SpawnPoint[i].position, SpawnPoint[i].rotation);
            StartCoroutine(SpawnStructure());
        }
    }

    //Torch
    IEnumerator DelayTorch()
    {
        yield return new WaitForSeconds(delayt);
        TorchOn = true;
    }

    IEnumerator SpawnTorch()
    {
        TorchOn = false;
        yield return new WaitForSeconds(cooldownt);
        TorchOn = true;
    }

    //Structure
    IEnumerator DelayStructure()
    {
        yield return new WaitForSeconds(delayinits);
        StructureOn = true;
    }

    IEnumerator SpawnStructure()
    {
        StructureOn = false;
        yield return new WaitForSeconds(delays);
        GameObject.Destroy(GameObject.FindGameObjectWithTag("Structure"));
        yield return new WaitForSeconds(cooldowns);
        StructureOn = true;
    }
}
