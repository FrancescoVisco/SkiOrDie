﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour {

    [Header("TimerSettings")]
    public int timeAmt;
    public float time;
    public bool TimerOff = false;
    public bool GameOn = false;
    private int scene;

    [Header("PanelSettings")]
    public GameObject StartPanel;
    public GameObject EndPanel;
    public float Delay;

	void Start () 
    {
       time = timeAmt;
       StartCoroutine(TimerOn());
	}
	

	void Update () 
    {
       if(time > 0 && TimerOff == false && GameOn == true)
       {
           time -= Time.deltaTime;
       }

       if(time < 0)
       {
           StartCoroutine(DelayOn()); 
       }

       if(TimerOff == true)
       {
           GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
       }
	}

    IEnumerator TimerOn()
    {
       yield return new WaitForSeconds(Delay);
       GameOn = true;
    }

    IEnumerator DelayOn()
    {
       yield return new WaitForSeconds(Delay);
       TimerOff = true; 
    }
}