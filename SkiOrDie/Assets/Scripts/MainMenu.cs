﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{

    public GameObject[] Panel; 
    public bool SpacebarOn;
    public GameObject[] Spacebar;
    public GameObject Text;
    public bool TextActivated;

    void Start()
    {
        
    }

    void Update()
    {
       if(Spacebar[0].active && SpacebarOn == true)
       {
           if(Input.GetKeyDown(KeyCode.Space))
           {
               Panel[0].SetActive(false);
               Panel[1].SetActive(true);
               StartCoroutine(Delay());
           }
       }

       if(Spacebar[1].active && SpacebarOn == true)
       {
           if(Input.GetKeyDown(KeyCode.Space))
           {
               Panel[1].SetActive(false);
               Panel[2].SetActive(true);
               StartCoroutine(Delay());
           }
       }

       if(Panel[2].active && SpacebarOn == true)
       {
           
           if(GameObject.Find("Panel2").GetComponent<WebcamController>().inputLight == false)
           {
               Text.SetActive(true);
               TextActivated = true;
           }

           if(TextActivated == true && GameObject.Find("Panel2").GetComponent<WebcamController>().inputLight == true)
           {
               Spacebar[2].SetActive(true);
           }

           if(Input.GetKeyDown(KeyCode.Space) && Spacebar[2].active)
           {
               Panel[2].SetActive(false);
               Panel[3].SetActive(true);
               StartCoroutine(Delay());
           }
       }

    if(Spacebar[3].active && SpacebarOn == true)
       {
           if(Input.GetKeyDown(KeyCode.Space))
           {
               StartCoroutine(Delay());
               GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
           }
       }
    }

    IEnumerator Delay()
    {
        SpacebarOn = false;
        yield return new WaitForSeconds(1f);
        SpacebarOn = true;
    }
}
