﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTorch : MonoBehaviour
{

    public GameObject TorchLight;
    public bool ActiveOn = false;

    void Start()
    {
        
    }

    void Update()
    {
        if (TorchLight.activeInHierarchy == true)
        {
            ActiveOn = true;
        }
        else
        {
            ActiveOn = false;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.CompareTag("Light") && ActiveOn == false)
        {
            Destroy(col.gameObject);
            TorchLight.SetActive(true);
        }
    }


}
