﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructureNoTorch : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            GameObject.Find("Spawner").GetComponent<SpawnManager>().PlayerInStructure = true;
        }

        if(other.gameObject.CompareTag("Torch"))
        {
            other.gameObject.SetActive(false);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            GameObject.Find("Spawner").GetComponent<SpawnManager>().PlayerInStructure = false;
        }
    }
}
